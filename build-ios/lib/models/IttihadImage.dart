class IttihadImage {
  String file;
  String name;
  String imageExt;
  IttihadImage({this.file, this.name, this.imageExt});
  Map<String, String> toJson() =>
      {'file': file, 'name': name, 'extension': imageExt};
  factory IttihadImage.fromJson(Map<String, dynamic> json) {
    return IttihadImage(
      file: json['file'],
      name: json['name'],
      imageExt: json['extension'],
    );
  }
}
