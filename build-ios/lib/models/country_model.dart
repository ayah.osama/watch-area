

class Country {

  int countryID;
  int deleted;
  String countryCode;
  String countryNameFirstL;
  String countryNameSecondL;
  String countryName;
  String countryKey;
  bool notActive;
  String descriptionFirstL;
  String descriptionSecondL;
  String countryLogo;
  dynamic countryLogoArray;
  bool isAvatarLogo;



  Country({
    this.countryID,
    this.deleted,
    this.countryCode,
    this.countryNameFirstL,
    this.countryNameSecondL,
    this.countryName,
    this.countryKey,
    this.notActive,
    this.descriptionFirstL,
    this.descriptionSecondL,
    this.countryLogo,
    this.countryLogoArray,
    this.isAvatarLogo,
  });

  Country.fromJson(Map<String, dynamic> json) {
    countryID = json['countryID'];
    deleted = json['deleted'];
    countryCode = json['countryCode'];
    countryNameFirstL = json['countryNameFirstL'];
    countryNameSecondL = json['countryNameSecondL'];
    countryName = json['countryName'];
    countryKey = json['countryKey'];
    notActive = json['notActive'];
    descriptionFirstL = json['descriptionFirstL'];
    descriptionSecondL = json['descriptionSecondL'];
    countryLogo = json['countryLogo'];
    countryLogoArray = json['countryLogoArray'];
    isAvatarLogo = json['isAvatarLogo'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['countryID'] = this.countryID;
    data['deleted'] = this.deleted;
    data['countryCode'] = this.countryCode;
    data['countryNameFirstL'] = this.countryNameFirstL;
    data['countryNameSecondL'] = this.countryNameSecondL;
    data['countryName'] = this.countryName;
    data['countryKey'] = this.countryKey;
    data['notActive'] = this.notActive;
    data['descriptionFirstL'] = this.descriptionFirstL;
    data['descriptionSecondL'] = this.descriptionSecondL;
    data['countryLogo'] = this.countryLogo;
    data['countryLogoArray'] = this.countryLogoArray;
    data['isAvatarLogo'] = this.isAvatarLogo;

    return data;
  }

}

