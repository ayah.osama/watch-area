// To parse this JSON data, do
//
//     final cityModel = cityModelFromJson(jsonString);

import 'dart:convert';

City cityModelFromJson(String str) => City.fromJson(json.decode(str));

String cityModelToJson(City data) => json.encode(data.toJson());

class City {
  City({
    this.cityId,
    this.deleted,
    this.cityCode,
    this.countryId,
    this.cityNameFirstL,
    this.cityNameSecondL,
    this.notActive,
    this.cityName,
    this.descriptionFirstL,
    this.descriptionSecondL,
    this.countryName,
    this.countryNameLang,
  });

  int cityId;
  dynamic deleted;
  dynamic cityCode;
  dynamic countryId;
  dynamic cityNameFirstL;
  dynamic cityNameSecondL;
  bool notActive;
  dynamic cityName;
  dynamic descriptionFirstL;
  dynamic descriptionSecondL;
  String countryName;
  dynamic countryNameLang;

  factory City.fromJson(Map<String, dynamic> json) => City(
    cityId: json["cityID"] == null ? null : json["cityID"],
    deleted: json["deleted"],
    cityCode: json["cityCode"],
    countryId: json["countryID"],
    cityNameFirstL: json["cityNameFirstL"],
    cityNameSecondL: json["cityNameSecondL"],
    notActive: json["notActive"] == null ? null : json["notActive"],
    cityName: json["cityName"],
    descriptionFirstL: json["descriptionFirstL"],
    descriptionSecondL: json["descriptionSecondL"],
    countryName: json["countryName"] == null ? null : json["countryName"],
    countryNameLang: json["countryNameLang"],
  );

  Map<String, dynamic> toJson() => {
    "cityID": cityId == null ? null : cityId,
    "deleted": deleted,
    "cityCode": cityCode,
    "countryID": countryId,
    "cityNameFirstL": cityNameFirstL,
    "cityNameSecondL": cityNameSecondL,
    "notActive": notActive == null ? null : notActive,
    "cityName": cityName,
    "descriptionFirstL": descriptionFirstL,
    "descriptionSecondL": descriptionSecondL,
    "countryName": countryName == null ? null : countryName,
    "countryNameLang": countryNameLang,
  };
}
