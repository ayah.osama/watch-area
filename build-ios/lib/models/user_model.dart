

class User {

  int id;
  String email;

  User({this.id,
  this.email
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    return data;
  }

  @override
  String toString() {
    return '"id" : {$id}, "email" : $email';
  }
}

