import 'dart:convert';

import 'package:ittihad/models/IttihadImage.dart';
import 'package:ittihad/models/IttihadVoucher.dart';

class IttihadUser {
  int id;
  String name;
  String code;
  String email;
  String gender;
  int countryId;
  int cityId;
  String phoneNumber;
  String imagePath;
  String cityName;
  String countryName;
  bool isVerfied;
  String firstVerifyDate;
  String updateVerifyDate;
  String dateOfBirth;
  String birthdate;
  bool isActive;
  String createdDate;
  String updateDate;
  List<IttihadVoucher> vouchers;
  IttihadImage userImage;
  bool fromCache;
  bool isRegistered;

  IttihadUser(
      {this.id,
      this.name,
      this.code,
      this.email,
      this.vouchers,
      this.userImage,
      this.birthdate,
      this.cityId,
      this.cityName,
      this.countryId,
      this.countryName,
      this.createdDate,
      this.dateOfBirth,
      this.firstVerifyDate,
      this.gender,
      this.imagePath,
      this.isActive,
      this.isVerfied,
      this.phoneNumber,
      this.updateDate,
      this.updateVerifyDate,
      this.isRegistered});

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'email': email,
        'gender': gender,
        'countryId': countryId,
        'cityId': cityId,
        'phoneNumber': phoneNumber,
        'imagePath': imagePath,
        'cityName': cityName,
        'countryName': countryName,
        'isVerfied': isVerfied,
        'isRegistered': isRegistered,
        'firstVerifyDate': firstVerifyDate,
        'updateVerifyDate': updateVerifyDate,
        'dateOfBirth': dateOfBirth,
        'birthdate': birthdate,
        'isActive': isActive,
        'createdDate': createdDate,
        'updateDate': updateDate,
        'imageData': userImage.toJson(),
        'voucherViewModelList': encondeVouchersToJson(vouchers)
      };

  factory IttihadUser.fromJson(Map<String, dynamic> json) {
    return IttihadUser(
      id: json['id'],
      birthdate: json['birthdate'],
      cityId: json['cityId'],
      countryId: json['countryId'],
      cityName: json['cityName'],
      countryName: json['countryName'],
      createdDate: json['createdDate'],
      dateOfBirth: json['dateOfBirth'],
      email: json['email'],
      firstVerifyDate: json['firstVerifyDate'],
      gender: json['gender'],
      imagePath: json['imagePath'],
      isActive: json['isActive'],
      isVerfied: json['isVerfied'],
      isRegistered: json['isRegistered'],
      name: json['name'],
      code: json['code'],
      phoneNumber: json['phoneNumber'],
      updateDate: json['updateDate'],
      updateVerifyDate: json['updateVerifyDate'],
      userImage: IttihadImage.fromJson(json['imageData']),
      vouchers: getUserVouchers(json['voucherViewModelList']),
    );
  }
}

getUserVouchers(List<dynamic> vouchers) {
  List<IttihadVoucher> ittiVouchers = new List<IttihadVoucher>();
  for (var voucher in vouchers) {
    ittiVouchers.add(IttihadVoucher.fromJson(voucher));
  }
  return ittiVouchers;
}

List encondeVouchersToJson(List<IttihadVoucher> list) {
  List voucherViewModelList = List();
  list.map((item) => voucherViewModelList.add(item.toJson())).toList();
  return voucherViewModelList;
}
