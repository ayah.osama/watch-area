class IttihadVoucher {
  int id;
  String name;
  String code;
  String userEmail;
  int perecentage;
  String description;
  String releaseDate;
  String expiredate;
  String instruction;
  int userId;

  IttihadVoucher(
      {this.id,
      this.code,
      this.description,
      this.expiredate,
      this.instruction,
      this.name,
      this.perecentage,
      this.releaseDate,
      this.userEmail,
      this.userId});
  factory IttihadVoucher.fromJson(Map<String, dynamic> json) {
    return new IttihadVoucher(
      code: json['code'],
      description: json['description'],
      expiredate: json['expiredate'],
      id: json['id'],
      instruction: json['instruction'],
      name: json['name'],
      perecentage: json['perecentage'],
      releaseDate: json['releaseDate'],
      userEmail: json['userEmail'],
      userId: json['userId'],
    );
  }
  Map<String, dynamic> toJson() => {
        'code': this.code,
        'description': this.description,
        'expiredate': this.expiredate,
        'id': this.id,
        'instruction': this.instruction,
        'name': this.name,
        'perecentage': this.perecentage,
        'releaseDate': this.releaseDate,
        'userEmail': this.userEmail,
        'userId': this.userId,
      };
}
