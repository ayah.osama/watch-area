import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/screens/registerScreen.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:ittihad/widgets/mainButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';
import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerificationCode extends StatefulWidget {
  final String userMail;

  VerificationCode({this.userMail});

  @override
  _VerificationCodeState createState() => _VerificationCodeState();
}

class _VerificationCodeState extends State<VerificationCode> {
  AuthService _authService = new AuthService();

  // final LocalStorage storage = new LocalStorage('mail');
  // StorageManager userStore = StorageManager();

  UserService userService = new UserService();
  IttihadUser _ittihadUser;

  @override
  void initState() {
    super.initState();
    userService.getUser(widget.userMail).then((user) => {
          if (user != null)
            {
              setState(() => {_ittihadUser = user})
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c3345),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width - 60,
              height: MediaQuery.of(context).size.height / 3,
              child: Image.asset(
                'assets/images/ittihad.png',
              ),
            ),
            Text(
              'Please Enter Verification Code',
              style: TextStyle(
                fontSize: 14,
                color: Colors.white,
              ),
            ),
            VerificationCodeInput(
              textStyle: TextStyle(color: Colors.white, fontSize: 18),
              keyboardType: TextInputType.text,
              itemDecoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                      color: Color(0xfffef201),
                      width: 0.5,
                      style: BorderStyle.solid),
                ),
              ),
              autofocus: false,
              itemSize: 60,
              onCompleted: (String value) {
                if (_ittihadUser != null) {
                  if (_ittihadUser.code.toLowerCase() == value.toLowerCase()) {
                    UserService service = new UserService();
                    _ittihadUser.isVerfied = true;

                    service.saveUser(this._ittihadUser);

                    this
                        ._authService
                        .registerIttihadUser(this._ittihadUser)
                        .then((response) async {
                      if (response.data["status"] == 200) {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        await prefs.setBool('isVerfied', true);
                      }
                    });

                    if (_ittihadUser.isRegistered == true) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VoucherScreen()));
                    } else {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegisterScreen(
                                    ittihadUser: this._ittihadUser,
                                  )));
                    }
                  } else {
                    Get.snackbar('Error', 'Verification Code is not valid');
                  }
                }
              },
            ),
            MainButton(
              title: 'Resend',
              titleColor: Color(0xff000000),
              fontSize: 18,
              backgroundColor: Color(0xfffef201),
              width: MediaQuery.of(context).size.width - 60,
              height: 50,
              borderRadius: 3,
              onPressed: () {
                print('verification button');
                this
                    .userService
                    .getUserFromAPI(_ittihadUser.email)
                    .then((usr) => () {
                          if (usr != null) {
                            Get.snackbar('Success',
                                'Verification Code is sent check your email');
                            setState(() => {_ittihadUser = usr});
                          } else {
                            Get.snackbar(
                                'Error', 'Verification Code has Error');
                          }
                        });
              },
            ),
          ],
        ),
      ),
    );
  }
}
