import 'package:ittihad/bloC/SendEmailScreen/send_email_cubit.dart';
import 'package:ittihad/bloC/SendEmailScreen/send_email_state.dart';
import 'package:ittihad/models/user_model.dart';
import 'package:ittihad/screens/verificationCodeScreen.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:ittihad/widgets/mainButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';

class VerificationScreen extends StatefulWidget {
  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c3345),
      body: BlocProvider(
        create: (context) => SendEmailCubit(),
        child: BlocConsumer<SendEmailCubit, SendEmailState>(
          builder: (context, state) {
            if (state is SendEmailInitial) {
              return _email(context);
            }
            if (state is SendEmailLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Text(state.toString());
          },
          listener: (context, state) {
            if (state is SendEmailSuccess) {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => VerificationCode()));
            }
            if (state is SendEmailError) {
              Get.snackbar('Error', state.errorMessage);
            }
          },
        ),
      ),
    );
  }
}

Widget _email(BuildContext context) {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  return Center(
    child: Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'assets/images/ittihad.png',
          ),
          Container(
            width: MediaQuery.of(context).size.width - 60,
            child: TextFormField(
              controller: emailController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter valid email';
                }
                return null;
              },
              decoration: new InputDecoration(
                border: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                    ),
                    borderRadius: BorderRadius.circular(3.0)),
                fillColor: Color(0xff404655),
                filled: true,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3.0),
                  borderSide: BorderSide(
                    color: Colors.yellow,
                  ),
                ),
                labelText: 'E-mail',
                labelStyle: TextStyle(
                  color: Color(0xffffffff),
                ),
              ),
            ),
          ),
          MainButton(
            title: 'Send verification code',
            titleColor: Color(0xff000000),
            fontSize: 18,
            backgroundColor: Color(0xfffef201),
            width: MediaQuery.of(context).size.width - 60,
            height: 50,
            borderRadius: 3,
            onPressed: () {
              print('test sen email');
              String email = emailController.text;
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => VerificationCode()));
              if (_formKey.currentState.validate()) {
                // context.bloc<SendEmailCubit>().checkEmail(email, context);
                BlocProvider.of<SendEmailCubit>(context)
                    .checkEmail(email, context);
              } else {
                print('not valid');
              }
            },
          ),
        ],
      ),
    ),
  );
}
