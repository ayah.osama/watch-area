import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ittihad/models/city_model.dart';
import 'package:ittihad/models/country_model.dart';
import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/screens/profile.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/remote/country_service.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:ittihad/widgets/datePicker.dart';
import 'package:ittihad/widgets/inputTextWidget.dart';
import 'package:ittihad/widgets/mainButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SideDrawer.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  UserService _userService = new UserService();
  final LocalStorage storage = new LocalStorage('mail');
  List<dynamic> vouchers = [];
  StorageManager userStore = StorageManager();

  IttihadUser _user;

  final _formKey = GlobalKey<FormState>();

  final dateController = TextEditingController.fromValue(TextEditingValue(
    text: '2020-01-20',
  ));
  TextEditingController _phoneController =
      new TextEditingController.fromValue(TextEditingValue(
    text: '',
  ));

  TextEditingController _nameController =
      new TextEditingController.fromValue(TextEditingValue(
    text: "",
  ));
  RegionService _regionService = new RegionService();
  AuthService _authService = new AuthService();
  String _value;
  List<Country> countriesList = [];
  List<City> citiesList = [];
  int selectedCountry;
  int selectedCity;
  File _image;
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    fetchAllData();
    // _value = _genderItems.first;
  }

  Future<void> fetchAllData() async {
    await getCountries();
    await getUser();
    await getCities();

    getCountries();
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  getCities() {
    this.selectedCity = null;
    this._regionService.getCities(selectedCountry.toString()).then((response) {
      if (response.data["status"] == 200) {
        setState(() {
          print('valid');
          for (dynamic jsonCity in response.data['data']) {
            this.citiesList.add(City.fromJson(jsonCity));
          }
          //comment this after return countryID on user profile
          selectedCity = this.citiesList.first.cityId;
        });
      } else {
        Get.snackbar('Error', 'No Cities');
      }
    });
  }

  getCountries() {
    this._regionService.getCountries().then((response) {
      if (response.data["status"] == 200) {
        setState(() {
          print('valid');
          for (dynamic jsonCountry in response.data['data']) {
            this.countriesList.add(Country.fromJson(jsonCountry));
          }
          //comment this after return countryID on user profile
          selectedCountry = this.countriesList.first.countryID;
        });
      } else {
        Get.snackbar('Error', 'No Countries');
      }
    });
  }

  final List<String> _genderItems = ['Gender', 'Male', 'Female'];

  void _onDropDownChanged(String value) {
    setState(() {
      _value = value;
    });
  }

  Future<void> getUser() async {
    IttihadUser user = await _userService.getUser(null);
    setState(() {
      _user = user;
      _nameController.text = _user.name;
      _phoneController.text = _user.phoneNumber;
      dateController.text = _user.birthdate;
      // selectedCity = user.cityId;
      // selectedCountry = user.countryId;
      _value = _user.gender;
      print(user);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(
        user: _user,
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Edit profile",
          style: TextStyle(
              color: Color(0xff99969e),
              fontWeight: FontWeight.bold,
              fontSize: 16),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Color(0xff99969e), size: 30),
          onPressed: () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Profile())),
        ),
        elevation: 0,
        backgroundColor: Color(0xff2c3345),
      ),
      backgroundColor: Color(0xff2c3345),
      body: SingleChildScrollView(
        child: _user != null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Center(
                    child: Column(
                      children: [
                        GestureDetector(
                          child: _image != null
                              ? Container(
                                  width: 150,
                                  height: 150,
                                  margin: EdgeInsets.only(top: 30),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                        color: Colors.yellow,
                                      )),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image.file(_image)))
                              : Container(
                                  width: 200,
                                  height: 200,
                                  margin: EdgeInsets.only(top: 50),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                        color: Colors.yellow,
                                      )),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: _user.userImage != null
                                        ? Image.memory(base64Decode(_user
                                            .userImage.file
                                            .toString()
                                            .split(',')[1]))
                                        : Image.asset('assets/images/user.jpg'),
                                  )),
                          onTap: getImageFromGallery,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            InputTextWidget(
                              controller: _nameController,
                              // labelText: 'Name',
                              validationMessage: "this name is not valid",
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DatePickerWidget(
                              initialValue: "test",
                              dateController: this.dateController,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DropdownButtonHideUnderline(
                              child: Container(
                                color: Color(0xff404655),
                                width: MediaQuery.of(context).size.width - 60,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 5),
                                child: DropdownButton<String>(
                                  dropdownColor: Color(0xff404655),
                                  iconEnabledColor: Colors.white,
                                  value: _value,
                                  items: _genderItems.map((value) {
                                    return DropdownMenuItem<String>(
                                        child: Text(
                                          value,
                                          style: TextStyle(color: Colors.white),
                                          // style: TextStyle(color: Colors.white),
                                        ),
                                        value: value);
                                  }).toList(),
                                  onChanged: _onDropDownChanged,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // DropdownButtonHideUnderline(
                            //   child: Container(
                            //     color: Color(0xff404655),
                            //     width: MediaQuery.of(context).size.width - 60,
                            //     padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                            //     child: DropdownButton(
                            //         dropdownColor: Color(0xff404655),
                            //         iconEnabledColor: Colors.white,
                            //         value: selectedCountry,
                            //         items: countriesList.map((Country country) {
                            //           return new DropdownMenuItem<int>(
                            //             value: country.countryID,
                            //             child: new Text(country.countryName,
                            //               style: TextStyle(color: Colors.white),),
                            //           );
                            //         }).toList(),
                            //         onChanged: (value) {
                            //           setState(() {
                            //             selectedCountry = value;
                            //             if(selectedCountry !=null)
                            //               this.getCities();
                            //           });
                            //         }),
                            //   ),
                            // ),
                            SizedBox(
                              height: 20,
                            ),
                            // this.selectedCountry != null ?
                            // DropdownButtonHideUnderline(
                            //   child: Container(
                            //     color: Color(0xff404655),
                            //     width: MediaQuery.of(context).size.width - 60,
                            //     padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                            //     child: DropdownButton(
                            //         dropdownColor: Color(0xff404655),
                            //         iconEnabledColor: Colors.white,
                            //         value: selectedCity,
                            //         items: citiesList.map((City city) {
                            //           return new DropdownMenuItem<int>(
                            //             value: city.cityId,
                            //             child:
                            //              Text(city.cityName ?? city.cityId.toString(),
                            //                 style: TextStyle(color: Colors.white),),
                            //             );
                            //         }).toList(),
                            //         onChanged: (value) {
                            //           setState(() {
                            //             selectedCity = value;
                            //           });
                            //         }),
                            //   ),
                            // )
                            //     : Container(),
                            // SizedBox(
                            //   height: this.selectedCountry != null ? 20 : 0,
                            // ),
                            InputTextWidget(
                              inputType: TextInputType.phone,
                              // initialValue: "0101010101010",
                              controller: _phoneController,
                              // labelText: 'Name',
                              validationMessage: "this phone is not valid",
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: MainButton(
                                title: 'Save',
                                titleColor: Color(0xff000000),
                                fontSize: 18,
                                backgroundColor: Color(0xfffef201),
                                width: MediaQuery.of(context).size.width - 60,
                                height: 50,
                                borderRadius: 3,
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    this._user.name =
                                        _nameController.text.toString();
                                    this._user.gender = _value;
                                    this._user.birthdate =
                                        dateController.text.toString();
                                    // this._user.countryName =
                                    //     _countryController.text.toString();
                                    // this._user.cityName =
                                    //     _cityController.text.toString();
                                    this._user.phoneNumber =
                                        _phoneController.text.toString();
                                    if (_image != null) {
                                      this._user.userImage.file =
                                          'data:image/jpeg;base64,' +
                                              base64Encode(
                                                  _image.readAsBytesSync());
                                    }
                                    UserService service = new UserService();

                                    service.saveUser(this._user);

                                    this
                                        ._authService
                                        .registerIttihadUser(this._user)
                                        .then((response) async {
                                      print(response.data["status"]);
                                      if (response.data["status"] == 200) {


                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        await prefs.setBool('isVerfied', true);
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Profile()),
                                        ); Get.snackbar(
                                            'Success', 'Your info is updated');
                                      } else {
                                        Get.snackbar(
                                            'Error', 'Server connection Error');
                                      }
                                    });
                                  } else {
                                    Get.snackbar('Error', 'Failed to register');
                                  }
                                  // Navigator.push(context,
                                  //     MaterialPageRoute(builder: (context) => Profile()));
                                  // if (_formKey.currentState.validate()) {
                                  // } else {
                                  // }
                                },
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              )
            : Container(),
      ),
    );
  }
}
