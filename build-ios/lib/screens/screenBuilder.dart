import 'package:ittihad/screens/SideDrawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ScreenBuilder extends StatefulWidget {
  @override
  _ScreenBuilderState createState() => _ScreenBuilderState();
}

class _ScreenBuilderState extends State<ScreenBuilder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c3345),
      drawer: SideDrawer(),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff2c3345),
      ),
      body: Center(
        child: Text('Side Menu'),
      ),
    );
  }
}
