import 'package:ittihad/models/IttihadVoucher.dart';
import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'SideDrawer.dart';

class VoucherScreen extends StatefulWidget {
  @override
  _VoucherScreenState createState() => _VoucherScreenState();
}

class _VoucherScreenState extends State<VoucherScreen> {
  // AuthService _authService = new AuthService();
  // final LocalStorage storage = new LocalStorage('mail');
  List<IttihadVoucher> vouchers = [];
  StorageManager userStore = StorageManager();

  UserService _userService = new UserService();
  IttihadUser _user;

  @override
  void initState() {
    super.initState();
    // userStore.getUserInfo().then((user) {
    //   print(user);
    //   this._authService.getVouchers(user.email).then((response) {
    //     print(response);
    //     print(user);
    //     this.vouchers = response.data['data'];
    //     if (response.data["status"] == 200) {
    //       setState(() {
    //         print(response.data);
    //         this.vouchers = response.data['data'];
    //       });
    //     } else {
    //       Get.snackbar('Error', 'Vouchers has error');
    //     }
    //   });
    // });
    _userService.getUser(null).then((user) {
      setState(() {
        _user = user;
        vouchers = _user.vouchers;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(
        user: _user,
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Ittihad Vouchers",
          style: TextStyle(
              color: Color(0xff99969e),
              fontWeight: FontWeight.bold,
              fontSize: 16),
        ),
        elevation: 0,
        backgroundColor: Color(0xff2c3345),
      ),
      backgroundColor: Color(0xff2c3345),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                child: Text(
                  "My Vouchers",
                  style: TextStyle(
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: vouchers.length,
                          itemBuilder: _buildVoucher,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildVoucher(BuildContext context, int index) {
    print(vouchers[index]);
    return Row(children: [
      Column(
        children: [
          Center(
            child: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text(
                                  vouchers[index].name,
                                  style: TextStyle(
                                      color: Color(0xff000000),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 7,
                                ),
                                Text(
                                  vouchers[index].perecentage.toString() +
                                      '% OFF',
                                  style: TextStyle(
                                    color: Color(0xffed2c19),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            Wrap(
                              children: [
                                Container(
                                  width: 200,
                                  child: Text(
                                    vouchers[index].description,
                                    style: TextStyle(
                                        color: Color(0xff000000), fontSize: 15),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  'Expiry date : ',
                                  style: TextStyle(
                                      color: Color(0xff000000), fontSize: 15),
                                ),
                                Text(
                                  vouchers[index].expiredate,
                                  style: TextStyle(
                                      color: Color(0xff000000), fontSize: 15),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ],
                ),
              ),
              width: MediaQuery.of(context).size.width - 60,
              height: MediaQuery.of(context).size.height / 4,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: const Color(0xff7c94b6),
                image: new DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: new ColorFilter.mode(
                      Colors.white.withOpacity(0.6), BlendMode.dstATop),
                  image: new AssetImage(
                    'assets/images/group_381.png',
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            child: Container(
              // width: MediaQuery.of(context).size.width - 40,
              width: MediaQuery.of(context).size.width - 60,
              // height: MediaQuery.of(context).size.height / 4,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: new BoxDecoration(
                  color: Color(0xff404655),
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        vouchers[index].name,
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        vouchers[index].perecentage.toString() + ' % OFF',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(height: 30),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Description: ",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Wrap(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 120,
                              child: Text(
                                vouchers[index].description,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 8),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Release date : " + vouchers[index].releaseDate,
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 15),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Expire date : " + vouchers[index].expiredate,
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 15),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Voucher code : " + (vouchers[index].code ?? ""),
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 8),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Instructions: ",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Wrap(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 120,
                              child: Text(
                                vouchers[index].instruction,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(vertical: 8),
                  ),
                ],
              ),
            ),
          ),
          //  SizedBox(
          //   width: 10,
          // ),
        ],
      )
    ]);
  }
}

class StateService {
  static final List<String> states = [
    'ANDAMAN AND NICOBAR ISLANDS',
    'ANDHRA PRADESH',
    'ARUNACHAL PRADESH',
    'ASSAM',
    'BIHAR',
    'CHATTISGARH',
    'CHANDIGARH',
    'DAMAN AND DIU',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(states);
    matches.retainWhere((s) => s.contains(query.toLowerCase()));
    return matches;
  }
}
