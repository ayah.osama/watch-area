import 'dart:async';
import 'package:ittihad/screens/verificationScreen.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:after_layout/after_layout.dart';
import 'package:splashscreen/splashscreen.dart';

class HomePage extends StatefulWidget {
  HomePage({this.title});

  final String title;

  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AfterLayoutMixin<HomePage> {
  Future checkIsValidated() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('isVerfied') ?? false);

    Timer(
        Duration(seconds: 4),
        () => {
              if (_seen)
                {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                      builder: (context) => new VoucherScreen()))
                }
              else
                {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                      builder: (context) => new VerificationScreen()))
                }
            });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 0,
        navigateAfterSeconds: '',
        backgroundColor: Color(0xff2c3345),
        title: new Text(
          'Ittihad Vouchers',
          textScaleFactor: 2,
        ),
        image: Image.asset('assets/images/ittihad.png'),
        loadingText: Text(
          "Loading",
          style: new TextStyle(
            color: Colors.white,
          ),
        ),
        photoSize: 100.0,
        loaderColor: Colors.white,
    );
  }

  @override
  void afterFirstLayout(BuildContext context) => checkIsValidated();
}
