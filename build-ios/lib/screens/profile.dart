import 'dart:convert';

import 'package:get/get.dart';
import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/models/user_model.dart';
import 'package:ittihad/screens/editProfileScreen.dart';
import 'package:flutter/material.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:localstorage/localstorage.dart';

import 'SideDrawer.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final LocalStorage storage = new LocalStorage('mail');
  List<dynamic> vouchers = [];
  StorageManager userStore = StorageManager();

  IttihadUser _user;

  UserService _userService = new UserService();

  @override
  void initState() {
    super.initState();
    getUser();
  }

  void getUser() {
    // userStore.getUserInfo().then((user) {
    //   // print(user);
    //   this._authService.getUserByID(user.id).then((response) {
    //     //print(response);
    //     // // print(user);
    //     // _user = response.data;
    //     // print(response.data);
    //     // var data = response.data;
    //     // print(response.data);
    //     // print(_user);
    //     setState(() {
    //       _user = response.data['data'];
    //       print(_user);
    //     });
    //   });
    // });
    _userService.getUser(null).then((user) {
      setState(() {
        _user = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // getUser();
    //print(_user ?? _user['name']);
    //print(_user);
    return Scaffold(
      drawer: SideDrawer(
        user: _user,
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "My profile",
          style: TextStyle(
              color: Color(0xff99969e),
              fontWeight: FontWeight.bold,
              fontSize: 16),
        ),
        elevation: 0,
        backgroundColor: Color(0xff2c3345),
      ),
      backgroundColor: Color(0xff2c3345),
      body: SingleChildScrollView(
        child: _user != null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Center(
                    child: Column(
                      children: [
                        Container(
                            width: 150,
                            height: 150,
                            margin: EdgeInsets.only(top: 50),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Colors.yellow,
                                )),
                            child: _user != null
                                ? ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: _user.userImage != null
                                        ? Image.memory(base64Decode(_user
                                            .userImage.file
                                            .toString()
                                            .split(',')[1]))
                                        : Image.asset('assets/images/user.jpg'),
                                  )
                                : Container()),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          _user.name != null ? _user.name : '',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          _user.email != null ? _user.email : '',
                          style: TextStyle(color: Color(0xff99969e)),
                        ),
                        SizedBox(
                          height: 60,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 35),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 24),
                                decoration: new BoxDecoration(
                                    color: Color(0xff404655),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Personal Info",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 19,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    GestureDetector(
                                      onTap: () => {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    EditProfileScreen()))
                                      },
                                      child: Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                decoration: new BoxDecoration(
                                    color: Color(0xff404655),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Date of birth : " +
                                                    _user.birthdate ??
                                                '',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17),
                                          ),
                                        ],
                                      ),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 8),
                                    ),
                                    Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Gender : " + _user.gender ?? '',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17),
                                          ),
                                        ],
                                      ),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 8),
                                    ),
                                    // Container(
                                    //   child: Row(
                                    //     mainAxisAlignment:
                                    //         MainAxisAlignment.spaceBetween,
                                    //     children: [
                                    //       Text(
                                    //         "Country : " + _user["countryName"] ?? "",
                                    //         style: TextStyle(
                                    //             color: Colors.white, fontSize: 17),
                                    //       ),
                                    //     ],
                                    //   ),
                                    //   padding: EdgeInsets.symmetric(vertical: 8),
                                    // ),
                                    // Container(
                                    //   child: Row(
                                    //     mainAxisAlignment:
                                    //         MainAxisAlignment.spaceBetween,
                                    //     children: [
                                    //       Text(
                                    //         "City : ",
                                    //         style: TextStyle(
                                    //             color: Colors.white, fontSize: 17),
                                    //       ),
                                    //     ],
                                    //   ),
                                    //   padding: EdgeInsets.symmetric(vertical: 8),
                                    // ),
                                    Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Phone number : " +
                                                _user.phoneNumber,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17),
                                          ),
                                        ],
                                      ),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 8),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )
            : Container(),
      ),
    );
  }
}
