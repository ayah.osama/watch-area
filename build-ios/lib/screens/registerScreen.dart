import 'dart:convert';
import 'dart:io';

import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:ittihad/widgets/mainButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  final IttihadUser ittihadUser;

  RegisterScreen({this.ittihadUser});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  AuthService _authService = new AuthService();
  StorageManager userStore = StorageManager();

  File _image;
  final picker = ImagePicker();

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  int userId;
  String userMail;

  String _value;

  @override
  void initState() {
    super.initState();

    _value = _genderItems.first;

    userStore
        .getUserInfo()
        .then((value) => {userId = value.id, userMail = value.email});
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _countryController = new TextEditingController();
  TextEditingController _cityController = new TextEditingController();
  final dateController = TextEditingController();

  // final RegExp phoneRegex = new RegExp(r'^[6-9]\d{9}$');

  final List<String> _genderItems = ['Gender', 'Male', 'Female'];

  void _onDropDownChanged(String value) {
    setState(() {
      _value = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c3345),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Center(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 50),
                      decoration: new BoxDecoration(
                          // shape: BoxShape.circle,
                          // borderRadius: BorderRadius.circular(30)
                          ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: _image == null
                                ? Image.asset(
                                    'assets/images/ic_account_circle.png',
                                  )
                                : Image.file(
                                    _image,
                                    width: 90,
                                    height: 100,
                                  ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: MainButton(
                        title: "Add photo",
                        titleColor: Color(0xff000000),
                        fontSize: 16,
                        backgroundColor: Color(0xfffef201),
                        width: MediaQuery.of(context).size.width - 250,
                        height: 35,
                        borderRadius: 3,
                        onPressed: () {
                          getImageFromGallery();
                          // if (_formKey.currentState.validate()) {
                          // } else {
                          //   print('not valid');
                          // }
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              Center(
                  child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      controller: _nameController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid name';
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(3.0)),
                        fillColor: Color(0xff404655),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            color: Colors.yellow,
                          ),
                        ),
                        labelText: 'Name',
                        labelStyle: TextStyle(
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      validator: (value) {
                        if (value == null || value == '')
                          return "this date of birth not valid";
                        return null;
                      },
                      decoration: new InputDecoration(
                        hintText: 'Pick your Date',
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(3.0)),
                        fillColor: Color(0xff404655),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            color: Colors.yellow,
                          ),
                        ),
                        labelText: 'Date of birth',
                        labelStyle: TextStyle(
                          color: Color(0xffffffff),
                        ),
                      ),
                      readOnly: true,
                      controller: dateController,
                      onTap: () async {
                        var date = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime.now());
                        dateController.text = date.toString().substring(0, 10);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  DropdownButtonHideUnderline(
                    child: Container(
                      color: Color(0xff404655),
                      width: MediaQuery.of(context).size.width - 60,
                      padding: EdgeInsets.symmetric(
                          horizontal: 8, vertical: 5),
                      child: DropdownButton<String>(
                        dropdownColor: Color(0xff404655),
                        iconEnabledColor: Colors.white,
                        value: _value,
                        items: _genderItems.map((value) {
                          return DropdownMenuItem<String>(
                              child: Text(
                                value,
                                style: TextStyle(color: Colors.white),
                                // style: TextStyle(color: Colors.white),
                              ),
                              value: value);
                        }).toList(),
                        onChanged: _onDropDownChanged,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      controller: _countryController,
                      validator: (value) {
                        if (value == null || value == '')
                          return "this country not valid";
                        return null;
                      },
                      decoration: new InputDecoration(
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(3.0)),
                        fillColor: Color(0xff404655),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            color: Colors.yellow,
                          ),
                        ),
                        labelText: 'Country',
                        labelStyle: TextStyle(
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      controller: _cityController,
                      validator: (value) {
                        if (value == null || value == '')
                          return "this city not valid";
                        return null;
                      },
                      decoration: new InputDecoration(
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(3.0)),
                        fillColor: Color(0xff404655),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            color: Colors.yellow,
                          ),
                        ),
                        labelText: 'City',
                        labelStyle: TextStyle(
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      keyboardType: TextInputType.phone,
                      controller: _phoneController,
                      inputFormatters: [
                        new WhitelistingTextInputFormatter(
                            new RegExp(r'^[0-9]*$')),
                        new LengthLimitingTextInputFormatter(11)
                      ],
                      validator: (value) {
                        if (value == null || value == '')
                          return "this country not valid";
                        return null;
                      },
                      decoration: new InputDecoration(
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                            borderRadius: BorderRadius.circular(3.0)),
                        fillColor: Color(0xff404655),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            color: Colors.yellow,
                          ),
                        ),
                        labelText: 'Phone number',
                        labelStyle: TextStyle(
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              )),
              SizedBox(
                height: 25,
              ),
              Center(
                child: MainButton(
                  title: "Submit",
                  titleColor: Color(0xff000000),
                  fontSize: 18,
                  backgroundColor: Color(0xfffef201),
                  width: MediaQuery.of(context).size.width - 60,
                  height: 50,
                  borderRadius: 3,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      this.widget.ittihadUser.name =
                          _nameController.text.toString();
                      this.widget.ittihadUser.gender = _value;
                      this.widget.ittihadUser.birthdate =
                          dateController.text.toString();
                      this.widget.ittihadUser.countryName =
                          _countryController.text.toString();
                      this.widget.ittihadUser.cityName =
                          _cityController.text.toString();
                      this.widget.ittihadUser.phoneNumber =
                          _phoneController.text.toString();
                      if (_image != null) {
                        this.widget.ittihadUser.userImage.file =
                            'data:image/jpeg;base64,' +
                                base64Encode(_image.readAsBytesSync());

                      }
                      this.widget.ittihadUser.isRegistered = true;
                      UserService service = new UserService();

                      service.saveUser(this.widget.ittihadUser);

                      this
                          ._authService
                          .registerIttihadUser(this.widget.ittihadUser)
                          .then((response) async {
                        if (response.data["status"] == 200) {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          await prefs.setBool('isVerfied', true);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VoucherScreen()));
                        } else {
                          Get.snackbar('Error', 'Server connection Error');
                        }
                      });
                    } else {
                      Get.snackbar('Error', 'Failed to register');
                    }
                  },
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
