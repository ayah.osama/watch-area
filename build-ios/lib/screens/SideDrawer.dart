import 'dart:convert';

import 'package:ittihad/models/ittihadUser.dart';
import 'package:ittihad/screens/profile.dart';
import 'package:ittihad/screens/voucher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SideDrawer extends StatefulWidget {
  final IttihadUser user;
  SideDrawer({this.user});
  @override
  _SideDrawerState createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {

  @override
  void initState() {
    super.initState();
    // getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 30,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Center(
              child: Column(
            children: [
              Container(
                width: 150,
                height: 150,
                margin: EdgeInsets.only(top: 50),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    border: Border.all(
                      color: Colors.yellow,
                    )),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: (this.widget.user != null &&
                            this.widget.user.userImage != null)
                        ? Image.memory(base64Decode(this
                            .widget
                            .user
                            .userImage
                            .file
                            .toString()
                            .split(',')[1]))
                        : Image.asset('assets/images/user.jpg'),

                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                (this.widget.user != null && this.widget.user.name != null)
                    ? this.widget.user.name
                    : '',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                (this.widget.user != null && this.widget.user.email != null)
                    ? this.widget.user.email
                    : '',
                style: TextStyle(color: Color(0xff404655)),
              ),
              SizedBox(
                height: 80,
              ),
              ListTile(
                leading: Icon(
                  Icons.person,
                  color: Color(0xff000000),
                ),
                title: Text(
                  'Profile',
                  style: TextStyle(color: Color(0xff2c3345), fontSize: 18),
                ),
                onTap: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()))
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.card_giftcard,
                  color: Color(0xff000000),
                ),
                title: Text(
                  'My Vouchers',
                  style: TextStyle(color: Color(0xff2c3345), fontSize: 18),
                ),
                onTap: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => VoucherScreen()))
                },
              ),
            ],
          )),
        ],
      ),
    );
  }
}
