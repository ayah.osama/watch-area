import 'package:bloc/bloc.dart';
import 'package:ittihad/services/remote/auth_service.dart';
import 'package:ittihad/services/remote/userService.dart';
import 'package:ittihad/services/storage_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'send_email_state.dart';

class SendEmailCubit extends Cubit<SendEmailState> {
  SendEmailCubit() : super(SendEmailInitial());
  UserService _userService = new UserService();
  void checkEmail(String email, BuildContext context) {
    emit(SendEmailLoading());
    this._userService.getUserFromAPI(email).then((value) {
      if (value != null && value.email != null) {
        emit(SendEmailSuccess());
      } else {
        emit(SendEmailError("not found"));
        emit(SendEmailInitial());
      }
    }).catchError((value) {
      emit(SendEmailError(value));
    });
  }

  // void checkEmail(String email, BuildContext context) {
  //   StorageManager userStore = StorageManager();

  //   emit(SendEmailLoading());
  //   this._authService.verificationMail(email).then((value) {
  //     if (value.data["status"] == 200) {
  //       userStore.saveUserInfo(0, email);
  //       emit(SendEmailSuccess());
  //     } else {
  //       emit(SendEmailError("not found"));
  //       emit(SendEmailInitial());
  //     }
  //   }).catchError((value) {
  //     // emit(SendEmailError(value));
  //   });
  // }
}
