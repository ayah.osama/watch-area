import 'package:equatable/equatable.dart';

abstract class SendEmailState extends Equatable {
  const SendEmailState();
}

class SendEmailInitial extends SendEmailState {
  @override
  List<Object> get props => [];
}

class SendEmailSuccess extends SendEmailState {
  @override
  List<Object> get props => [];


}
class SendEmailLoading extends SendEmailState {
  @override
  List<Object> get props => [];
}
class SendEmailError extends SendEmailState {
  const SendEmailError(this.errorMessage);
  @override
  List<Object> get props => [];
  final String errorMessage;




}