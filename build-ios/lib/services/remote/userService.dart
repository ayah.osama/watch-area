import 'dart:convert';

import 'package:ittihad/models/ittihadUser.dart';
import 'package:localstorage/localstorage.dart';
import 'package:http/http.dart' as http;

class UserService {
  String baseUrl =
      // 'http://10.10.61.27:8000/api/Ithad/';
      'http://ittihad.itlink.support:81/api/Ithad/';
  static LocalStorage storage = new LocalStorage('ittihaduser');

  void saveUser(IttihadUser user) async {
    await storage.ready;
    storage.setItem("ittihaduser", user);
  }

  Future<IttihadUser> getUser(String email) async {
    var user = await getUserFromCache();
    if (user == null && email != null) {
      user = await getUserFromAPI(email);
    }
    return user;
  }

  Future<IttihadUser> getUserFromCache() async {
    await storage.ready;

    Map<String, dynamic> data = storage.getItem('ittihaduser');
    if (data == null) {
      return null;
    }
    IttihadUser user = IttihadUser.fromJson(data);
    user.fromCache = true;
    return user;
  }

  Future<IttihadUser> getUserFromAPI(String email) async {
    IttihadUser user = await fetchUser(email);
    if (user != null) {
      saveUser(user);
      return user;
    } else {
      return null;
    }
  }

  Future<IttihadUser> fetchUser(String email) async {
    var endPoint = 'VerifyEmail';
    dynamic user = await _get(endPoint, email);
    if (user == null) {
      return null;
    }
    IttihadUser usr = new IttihadUser.fromJson(user);
    return usr;
  }

  Future _get(String endpoint, String email) async {
    String url = '$baseUrl$endpoint?mail=$email';
    print(url);
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        var resbody = jsonDecode(response.body);
        return resbody['data'];
      } else {}
    } catch (e) {}
  }
}
