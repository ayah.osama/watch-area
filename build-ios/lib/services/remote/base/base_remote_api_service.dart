import 'package:dio/dio.dart';

abstract class BaseRemoteApiService {
  Future<Response> getWithParams(String api, Map<String, dynamic> params,
      {Map<String, dynamic> headers});

  Future<Response> postWithParams(String api, Map<String, dynamic> params,
      {Map<String, dynamic> headers});

// String generateURLWithParams(Map<String, dynamic> params, String api);

}
