import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'base_remote_api_service.dart';

@protected
class BaseRemoteApiServiceImp extends BaseRemoteApiService {
  Dio dio;
  String baseUrl =
      // 'http://10.10.61.27:8000/api/Ithad/';
      'http://ittihad.itlink.support:81/api/Ithad/';
  BaseRemoteApiServiceImp() : super() {
    BaseOptions options = BaseOptions(
        receiveTimeout: 150000, connectTimeout: 150000, followRedirects: false);
    dio = new Dio(options);
  }

  @protected
  @override
  Future<Response> getWithParams(String api, Map<String, dynamic> params,
      {Map<String, dynamic> headers}) async {
    String reqUrl = baseUrl + generateURLWithParams(params, api);
    print(reqUrl);
    return dio.get(reqUrl, queryParameters: params);
  }

  @protected
  @override
  Future<Response> postWithParams(String api, Map<String, dynamic> params,
      {Map<String, dynamic> headers}) async {
    String reqUrl = baseUrl + api;
    print(params);
    print(reqUrl);
    return dio.post(reqUrl, data: params);
  }

  // params = {
  //   "product":2,
  //   "category":1
  // }
  // api = 'http://localhost/product'
  // result = 'http://localhost/product?product=2&category=1'
  // result = 'http://localhost/product?product=2&category=1'
  // @protected
  // @override
  String generateURLWithParams(Map<String, String> params, String api) {
    api = (params != null && params.isNotEmpty) ? api + '?' : api;
    params.forEach((key, val) {
      if (val != null) {
        api += key + "=" + val + '&';
      }
    });
    return api;
  }
}
