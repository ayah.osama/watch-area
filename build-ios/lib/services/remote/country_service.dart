import 'package:dio/dio.dart';
import 'base/base_remote_api_service_imp.dart';


class RegionService extends BaseRemoteApiServiceImp {
  String countryAPI = 'GetCountries';
  String cityAPI = 'GetCities';


  Future<Response> getCountries() {
    dynamic params = {
    '':'',
    };
    return getWithParams(countryAPI, params);
  }

  Future<Response> getCities(String countryId) {
    dynamic params = {
      'countryId': countryId,
    };
    return getWithParams(cityAPI, params);
  }



}
