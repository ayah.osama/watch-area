import 'package:dio/dio.dart';
import 'package:ittihad/models/ittihadUser.dart';
import 'base/base_remote_api_service_imp.dart';

class AuthService extends BaseRemoteApiServiceImp {
  String verificationMailAPI = 'StartSendEmail';
  String verificationCodeAPI = 'ResendCode';
  String getVouchersAPI = 'GetVoucherList';
  String registerAPI = 'UpdateUnionUser';
  String userAPI = 'GetUserByID';

  Future<Response> verificationMail(String mail) {
    dynamic params = {
      'mail': mail,
    };
    return getWithParams(verificationMailAPI, params);
  }

  Future<Response> verificationCode(String email, String code) {
    dynamic params = {
      'email': email,
      'code': code,
    };
    return postWithParams(verificationCodeAPI, params);
  }

  Future<Response> getVouchers(String mail) {
    dynamic params = {
      'mail': mail,
    };
    return getWithParams(getVouchersAPI, params);
  }

  Future<Response> getUserByID(int id) {
    dynamic params = {
      'Id': id.toString(),
    };
    return getWithParams(userAPI, params);
  }

  Future<Response> register(
    int id,
    String name,
    String email,
    String gender,
    String countryName,
    String cityName,
    String phoneNumber,
    String dateOfBirth,
  ) {
    dynamic params = {
      'id': id,
      'name': name,
      'email': email,
      'gender': gender,
      'countryName': countryName,
      'cityName': cityName,
      'phoneNumber': phoneNumber,
      'dateOfBirth': dateOfBirth,
    };
    print(params);

    return postWithParams(registerAPI, params);
  }

  Future<Response> registerIttihadUser(IttihadUser user) {
    // user.vouchers.clear();

    return postWithParams(registerAPI, user.toJson());
  }
}
