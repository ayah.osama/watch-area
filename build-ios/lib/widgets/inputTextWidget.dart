import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputTextWidget extends StatelessWidget {
  final String labelText;
  final String validationMessage;
  TextEditingController controller;
  String initialValue;
  TextInputType inputType;

  InputTextWidget({
    Key key,
    this.labelText,
    this.validationMessage,
    this.controller,
    this.initialValue,
    this.inputType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 60,
      child: TextFormField(
        inputFormatters: inputType == TextInputType.phone ? [
          new WhitelistingTextInputFormatter(
              new RegExp(r'^[0-9]*$')),
          new LengthLimitingTextInputFormatter(11)
        ]: [],
        keyboardType: inputType,
        style: TextStyle(color: Colors.white),
        // initialValue: initialValue,
        controller: controller,
        validator: (value) {
          if (value == null || value == '') return validationMessage;
          return null;
        },
        decoration: new InputDecoration(
          border: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.blue,
              ),
              borderRadius: BorderRadius.circular(3.0)),
          fillColor: Color(0xff404655),
          filled: true,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(3.0),
            borderSide: BorderSide(
              color: Colors.yellow,
            ),
          ),
          labelText: labelText,
          labelStyle: TextStyle(
            color: Color(0xffffffff),
          ),
        ),
      ),
    );
  }
}
