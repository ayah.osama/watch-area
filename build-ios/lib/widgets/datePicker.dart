import 'package:flutter/material.dart';

class DatePickerWidget extends StatelessWidget {

  final dateController;
  final String labelText;
  final String validationMessage;
  String initialValue;

  DatePickerWidget({
    Key key,
    this.dateController,
    this.labelText,
    this.validationMessage,
    this.initialValue

  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  Container(
      width: MediaQuery.of(context).size.width - 60,
      child:

          TextFormField(
            // initialValue: initialValue,
            style: TextStyle(color: Colors.white),
            // keyboardType: TextInputType.datetime,
            validator: (value) {
              if (value == null || value == '')
                return validationMessage;
              return null;
            },
            decoration: new InputDecoration(
              // hintText: 'Pick your Date',
              border: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.blue,
                  ),
                  borderRadius: BorderRadius.circular(3.0)),
              fillColor: Color(0xff404655),
              filled: true,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(3.0),
                borderSide: BorderSide(
                  color: Colors.yellow,
                ),
              ),
              labelText: labelText,
              labelStyle: TextStyle(
                color: Color(0xffffffff),
              ),
            ),
            readOnly: true,
            controller: this.dateController,
            onTap: () async {
              var date = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(1900),
                  lastDate: DateTime.now());
              dateController.text = date.toString().substring(0, 10);
            },
          ),


    );
  }
}
