
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';



import 'base_remote_api_service.dart';

@protected
class BaseRemoteApiServiceImp extends BaseRemoteApiService{
  Dio dio;
  BaseRemoteApiServiceImp() : super(){
    BaseOptions options = BaseOptions(
        receiveTimeout: 30000,
        connectTimeout: 30000,
        followRedirects: false,
        baseUrl: 'http://localhost:8080/');
    dio = new Dio(options);

  }


  @protected
  @override
  Future<Response> getWithParams(String api, Map<String, dynamic> params,{Map<String, dynamic> headers}) async {
    // String reqUrl =  generateURLWithParams(params, api);
    return dio.get(api,queryParameters: params,options: Options(headers: headers));
  }

  @protected
  @override
  Future<Response> postWithParams(String api, Map<String, dynamic> params,{Map<String, dynamic> headers}) async {
    // String reqUrl =  generateURLWithParams(params, api);
    return dio.post(api, data: params,options: Options(headers: headers));
  }

  // params = {
  //   "product":2,
  //   "category":1
  // }
  // api = 'http://localhost/product'
  // result = 'http://localhost/product?product=2&category=1'
  // result = 'http://localhost/product?product=2&category=1'
  // @protected
  // @override
  // String generateURLWithParams(Map<String, dynamic> params, String api) {
  //   api = (params !=null && params.isNotEmpty) ? api+'?' : api;
  //   params.forEach((key, val) {
  //     if (val != null) {
  //       api += key + "=" + val + '&';
  //     }
  //   });
  //   return api;
  // }



}