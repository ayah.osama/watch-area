import 'package:watch_area/services/remote/base/base_remote_api_service_imp.dart';
import 'package:dio/dio.dart';

class AuthService extends BaseRemoteApiServiceImp {
  String loginApi = 'login';

  Future<Response> login(String username, String password) {
    dynamic params = {
      'username': username,
      'password': password,
    };
    return postWithParams(loginApi, params);
  }
}
