import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/CollectionDetails.dart';
import 'package:watch_area/UI/widgets/main_card.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:getwidget/getwidget.dart';


class CollectionListScreen extends StatefulWidget {
  @override
  _CollectionListScreenState createState() => _CollectionListScreenState();
}

class _CollectionListScreenState extends State<CollectionListScreen> {

  @override
  Widget build(BuildContext context) {
   void _showModalSheet() {
      showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          context: context,
          builder: (builder) {
            return Container(
              // height: MediaQuery.of(context).size.height * 0.75,
              child: Center(
                child:  Text('Hello From Modal Bottom Sheet'),
              ),
              padding: EdgeInsets.all(40.0),
            );
          });
    }

    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(1.0), // here the desired height
          child: HeaderState(
            title: "Collection List",
            showTitle: false,

          ),
        ),
        backgroundColor: const Color(0xfff5f5f5),
        body: ListView(
            // mainAxisSize: MainAxisSize.min,
            // mainAxisAlignment: MainAxisAlignment.spaceAround,

            children: <Widget>[
              // HeaderState(title: "Collection", showTitle: true, height: 43),
              Column(
                children: [
                  Container(
                    height: 50,
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: Icon(Icons.refresh),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Collection List',
                          style: TextStyle(fontSize: 22),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: GFIconButton(
                          onPressed: () {
                            print('test');
                            _showModalSheet();
                          },
                          icon: Icon(Icons.filter_alt, color: Colors.black),
                          type: GFButtonType.transparent,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CollectionDetails()));
                      },
                      child: MainCard(
                        cardId: 1,
                        // title: 'collection list',
                        // fontColor: Colors.black,
                        backgroundColor: Color(0xffffffff),
                        width: MediaQuery.of(context).size.width - 40,
                        // height: 100,
                        borderRadius: 20.1,
                        child:
                            CollectionCardChild(showMenu: false, statusID: 1),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: MainCard(
                      cardId: 1,
                      // title: 'collection list',
                      // fontColor: Colors.black,
                      backgroundColor: Color(0xffffffff),
                      width: MediaQuery.of(context).size.width - 40,
                      // height: 100,
                      borderRadius: 20.1,
                      child: CollectionCardChild(showMenu: false, statusID: 2),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: MainCard(
                      cardId: 1,
                      // title: 'collection list',
                      // fontColor: Colors.black,
                      backgroundColor: Color(0xffffffff),
                      width: MediaQuery.of(context).size.width - 40,
                      // height: 100,
                      borderRadius: 20.1,
                      child: CollectionCardChild(showMenu: false, statusID: 3),
                    ),
                  ),
                ],
              ),
            ]));
  }
}

class CollectionCardChild extends StatelessWidget {
  final bool showMenu;
  final int statusID;

  CollectionCardChild({
    Key key,
    this.showMenu,
    this.statusID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Collection Name',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Text('Event GUID: '),
            FittedBox(
              // 2
              fit: BoxFit.contain,
              // 3
              child: Text('eec45832-5922-41ee-a671-a'),
            )
          ],
        ),
        SizedBox(height: 8),
        Row(
          // crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                child: Row(
                  children: [
                    Text('Direction: '),
                    Text('Exit'),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                'Pending',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('03/11/2020 03:03 PM'),
            // showMenu == false
            //     ? Container(
            //         padding: EdgeInsets.all(5),
            //         color: Color(0xff7E2A6A).withOpacity(0.5),
            //         child: Icon(
            //           Icons.menu,
            //           size: 20,
            //         ),
            //       )
            //     : Container(),
          ],
        ),
      ],
    );
  }
}
