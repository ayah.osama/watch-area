import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/widgets/header.dart';



class CaptureScreen extends StatelessWidget {


  CaptureScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return

      Scaffold(
        appBar: PreferredSize(
        preferredSize: Size.fromHeight(8.0), // here the desired height
    child: HeaderState(title: "Capture", showTitle: false, height: 43),
    ),
    backgroundColor: const Color(0xffffffff),
    body: ListView(
      // crossAxisAlignment: CrossAxisAlignment.start,
      // mainAxisAlignment: MainAxisAlignment.end,
      children: [
        HeaderState(title: "Capture",showTitle: true, height: 20),

        GestureDetector(
          onTap: () {
            print('Captureeeeeeeeeeeeee');
          },
          child: titleContainer(context, 'Captureeeeeeeeeeeeee', Icons.settings_outlined),
        ),
        line(context),
      ],
    ),
    );
  }
}

Widget line(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(top: 10, bottom: 10),
    color: Color(0xff666666),
    width: MediaQuery.of(context).size.width-60,
    height: 1,
  );
}

Widget titleContainer(BuildContext context,String title, IconData icon) {
  return Container(
    width: MediaQuery.of(context).size.width-60,
    height: 65,
    child: Row(

      children: [
        Container(
          margin: EdgeInsets.only(right: 20),
          child:Icon(icon, size: 32,
            color: Color(0xff7E2A6A

            ),
          ),
        ),

        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            // color: Color(0xff7E2A6A),
          ),
        ),
      ],
    ),
  );
}
