import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:getwidget/getwidget.dart';
import 'package:watch_area/UI/screens/DeliveryNotesDetails.dart';

import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/main_card.dart';


class DeliveryNoteList extends StatefulWidget {
  @override
  _DeliveryNoteListState createState() => _DeliveryNoteListState();
}

class _DeliveryNoteListState extends State<DeliveryNoteList> {
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(1.0), // here the desired height
            child: HeaderState(
              title: "Delivery note list",
              showTitle: false,
            ),
          ),
          backgroundColor: const Color(0xfff5f5f5),
          body: ListView(
            // mainAxisSize: MainAxisSize.min,
            // mainAxisAlignment: MainAxisAlignment.spaceAround,

              children: <Widget>[
                // HeaderState(title: "Collection", showTitle: true, height: 43),
                Column(
                  children: [
                    Container(
                      height: 50,
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child:
                            Icon(Icons.refresh),),
                          SizedBox
                            (width: 10,),
                          Text('Delivery note',style: TextStyle(fontSize: 22),),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: GFIconButton(
                            onPressed: () {
                              print('test');
                            },
                            icon: Icon(Icons.filter_alt, color: Colors.black),
                            type: GFButtonType.transparent,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DeliveryNotesDetailsScreen()));
                        },
                        child: MainCard(
                          cardId: 1,
                          // title: 'collection list',
                          // fontColor: Colors.black,
                          backgroundColor: Color(0xffffffff),
                          width: MediaQuery.of(context).size.width - 40,
                          // height: 100,
                          borderRadius: 20.1,
                          child:
                          DeliveryNoteCardChild(showMenu: false, statusID: 1),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: MainCard(
                        cardId: 1,
                        // title: 'collection list',
                        // fontColor: Colors.black,
                        backgroundColor: Color(0xffffffff),
                        width: MediaQuery.of(context).size.width - 40,
                        // height: 100,
                        borderRadius: 20.1,
                        child: DeliveryNoteCardChild(showMenu: false, statusID: 2),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: MainCard(
                        cardId: 1,
                        // title: 'collection list',
                        // fontColor: Colors.black,
                        backgroundColor: Color(0xffffffff),
                        width: MediaQuery.of(context).size.width - 40,
                        // height: 100,
                        borderRadius: 20.1,
                        child: DeliveryNoteCardChild(showMenu: false, statusID: 3),
                      ),
                    ),
                  ],
                ),
              ]));
  }
}


class DeliveryNoteCardChild extends StatelessWidget {
  final bool showMenu;
  final int statusID;

  DeliveryNoteCardChild({
    Key key,
    this.showMenu,
    this.statusID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Delivery Note',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Text('Event GUID: '),
            FittedBox(
              // 2
              fit: BoxFit.contain,
              // 3
              child: Text('eec45832-5922-41ee-a671-a671-a'),
            )
          ],
        ),
        SizedBox(height: 8),
        Row(
          // crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                child: Row(
                  children: [
                    Text('Direction: '),
                    Text('Exit'),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                'Pending',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('03/11/2020 03:03 PM'),
            // showMenu == false
            //     ? Container(
            //         padding: EdgeInsets.all(5),
            //         color: Color(0xff7E2A6A).withOpacity(0.5),
            //         child: Icon(
            //           Icons.menu,
            //           size: 20,
            //         ),
            //       )
            //     : Container(),
          ],
        ),
      ],
    );
  }
}
