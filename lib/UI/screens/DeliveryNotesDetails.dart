import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/DeliveryNotesDetailsConfirm.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/subject_item.dart';
import 'package:get/get.dart';


class DeliveryNotesDetailsScreen extends StatefulWidget {
  @override
  _DeliveryNotesDetailsScreenState createState() => _DeliveryNotesDetailsScreenState();
}

class _DeliveryNotesDetailsScreenState extends State<DeliveryNotesDetailsScreen> {
  List<dynamic> subjectList1 = [
    {"subjectName":"Data Structure","done":true},
    {"subjectName":"Algorithms","done":true},
    {"subjectName":"Data Science","done":false},
  ];
  List<dynamic> subjectList2 = [
    {"subjectName":"Machine learning","done":true},
    {"subjectName":"compilers","done":true},
    {"subjectName":"graphics","done":true},
    {"subjectName":"prolog","done":true},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  AppBar(
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        backgroundColor: Colors.white,

        title: Container(
          child: Text("Delivery note details",style: TextStyle(color: Colors.black),),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * .1),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(height: MediaQuery.of(context).size.height * .06,),
                Center(
                  child: Column(
                    children: [
                      Text('Delivery note number 24 Arrived',style: TextStyle(fontWeight: FontWeight.bold),),
                      SizedBox(height: 10,),
                      Text('Vehicle number : 1234',style: TextStyle(color: Color(0xff7e2a6a),fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.05,),
                Text('That contain'),
                SizedBox(height: MediaQuery.of(context).size.height*.01,),
                Container(
                  color: Color(0xffe8e8e8),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Subject',style: TextStyle(color: Color(0xff7e2a6a),fontWeight: FontWeight.bold),),
                      SizedBox(height: MediaQuery.of(context).size.height*.02,),
                      for(dynamic subject in subjectList1)
                        SubjectItemWidget(subjectName: subject['subjectName'], done: subject['done'])

                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.02,),
                Container(
                  color: Color(0xffe8e8e8),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Subject',style: TextStyle(color: Color(0xff7e2a6a),fontWeight: FontWeight.bold),),
                      SizedBox(height: MediaQuery.of(context).size.height*.02,),
                      for(dynamic subject in subjectList2)
                        SubjectItemWidget(subjectName: subject['subjectName'], done: subject['done'])

                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.02,),
                Container(
                  child: Center(
                    child: Column(

                      children: [
                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: [
                             RaisedButton(
                               onPressed: (){},
                               textColor: Color(0xff7e2a6a),
                               color: Colors.white,
                               child: Text(
                                   "Return"
                               ),
                               shape:  RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(5.0),
                                   side: BorderSide(color: Color(0xff7e2a6a),)
                               ),
                             ),

                             SizedBox(width: MediaQuery.of(context).size.width * 0.03,),
                             RaisedButton(
                               onPressed: (){
                                 Get.to(DeliveryNotesDetailsConfirmScreen());
                               },
                               color: Color(0xff7e2a6a),
                               textColor: Colors.white,
                               child: Text(
                                   "Confirm"
                               ),
                               shape:  RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(5.0),

                               ),
                             ),
                           ],
                         )
                      ],
                    ),
                  ),
                )
              ],

            ),
          )


        ),
      ),
    );
  }
}
