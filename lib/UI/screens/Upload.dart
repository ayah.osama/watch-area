import 'dart:io';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/subject_item.dart';
import 'package:image_picker/image_picker.dart';

class UploadScreen extends StatefulWidget {
  @override
  _UploadScreenState createState() => _UploadScreenState();
}

class _UploadScreenState extends State<UploadScreen> {
  File _image;
  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * .05),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * .02,),
                Container(

                  child: GestureDetector(
                    child: Image.asset('assets/upload.png'),
                    onTap: getImageFromCamera,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.06,),
                Center(
                  child: Text('Tap to take photo',style: TextStyle(fontSize: 17),),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.06,),
                Center(
                  child: Text('OR',style: TextStyle(fontSize: 17),),
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.06,),
                Container(
                  child: Center(
                    child: Column(

                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RaisedButton(
                              onPressed: getImageFromGallery,
                              textColor: Color(0xff7e2a6a),
                              color: Colors.white,
                              child: Text(
                                  "Upload an image"
                              ),
                              shape:  RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color: Color(0xff7e2a6a),)
                              ),
                            ),


                          ],
                        ),
                        _image == null
                            ? Text('No image selected.')
                            : Image.file(_image,width: 100,),
                      ],
                    ),
                  ),
                )
              ],

            ),


          )


        ),
      ),
    );
  }
}
