import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:getwidget/getwidget.dart';

class CollectionDetails extends StatefulWidget {
  @override
  _CollectionDetailsState createState() => _CollectionDetailsState();
}

class _CollectionDetailsState extends State<CollectionDetails> {
  List<dynamic> _demoItems = [1, 2, 3];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        backgroundColor: Colors.white,

        title: Center(
          child: Text("Collection details",style: TextStyle(color: Colors.black),),
        ),
      ),
      backgroundColor: Color(0xffffffff),
      body:
      // ListView();

      new ListView.builder(
        itemCount: collectionDetailsList.length,
        itemBuilder: (context, i) {
          return
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 10,top:20),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xfff6f6f6),
                  ),
                  child: ExpansionTile(
                    leading: Text(
                      collectionDetailsList[i].title,
                      style: new TextStyle(
                        color: Color(0xff7e2a6a),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    tilePadding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    initiallyExpanded: true,
                    title: new Container(),
                    children: <Widget>[
                      new Column(
                        children:
                            _buildExpandableContent(collectionDetailsList[i]),
                      ),
                    ],
                  ),
                ),
              );
        },
      ),
    );
  }
}

_buildExpandableContent(Education education) {
  List<Widget> columnContent = [];
  for (String content in education.topic)
    columnContent.add(
      Column(children: [
        //   SizedBox(
        //   height: 8,
        // ),
        Container(
          margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          padding: EdgeInsets.all(10),
          // color: Colors.grey,
          child: ListTile(
            title: Text(
              "1234 KSA",
              style: new TextStyle(fontSize: 18.0),
            ),
            subtitle: Text(
              content,
              style: new TextStyle(fontSize: 18.0),
            ),
            leading: Image.asset(
              'assets/10810.png',
              // fit: BoxFit.cover,
            ),
          ),
        ),
      ]),
    );

  return columnContent;
}

class Education {
  final String title;
  List<String> topic = [];

  Education(this.title, this.topic);
}

List<Education> collectionDetailsList = [
  new Education('Vehciles', ['111']),
  new Education('Subjects', ['1111', '2']),
  new Education('Objects', ['111', '222', '333']),
];
