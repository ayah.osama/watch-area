import 'package:flutter/material.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/login_form.dart';
import 'package:watch_area/functions/getDeviceInfo.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(1.0), // here the desired height
        child: HeaderState(title: "DomainRegister", showTitle: false, height: 0),
      ),
      backgroundColor: const Color(0xffffffff),
      body: ListView(
        children: [
          // HeaderState(title: "Login",showTitle: false, height: 20),

          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: new Image.asset(
              'assets/small_logo.jpg',
              // width: 50.0,
              height: 90.0,
              // fit: BoxFit.cover,
            ),
          ),

          // FutureBuilder(
          //   future: deviceInfo(),
          //   builder: (BuildContext context, AsyncSnapshot snap) {
          //     if (snap.hasData) {
          //       return Text(snap.toString());
          //       your logic goes here.
              // } else {
              //   return new CircularProgressIndicator();
              // }
            // },
          // ),

          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: LoginForm(),
          ),
        ],
      ),
    );
  }
}
