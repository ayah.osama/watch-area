import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/CollectionList.dart';
import 'package:watch_area/UI/screens/DeliveryNotesDetails.dart';
import 'package:watch_area/UI/screens/Login.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/screen_builder.dart';

import 'DeliveryNoteList.dart';

class MoreScreen extends StatelessWidget {
  MoreScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(1.0), // here the desired height
        child: HeaderState(title: "More", showTitle: false, height: 0),
      ),
      backgroundColor: const Color(0xffffffff),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // HeaderState(title: "More",showTitle: false , height: 43),
            GestureDetector(
              onTap: () {
                print('test');
                // _onWillPop(context);
              },
              child:
                  titleContainer(context, 'Setting', Icons.settings_outlined),
            ),
            line(context),
            GestureDetector(
              onTap: () {
                print('click collection');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CollectionListScreen()));
              },
              child: titleContainer(context, 'Collection list', Icons.list),
            ),
            line(context),
            GestureDetector(
              onTap: () {
                print('click delivery notes');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DeliveryNoteList()));
              },
              child: titleContainer(context, 'Delivery note list', Icons.list),
            ),
            line(context),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    print('click emergency request');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CollectionListScreen()));
                  },
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Icon(
                          Icons.request_page_outlined,
                          size: 32,
                          color: Color(0xff7E2A6A),
                        ),
                      ),
                      Text(
                        "Emergency requests",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          // color: Color(0xff7E2A6A),
                        ),
                      ),
                    ],
                  ),
                ),
                Chip(
                  padding: EdgeInsets.all(3),
                  backgroundColor: Colors.red,
                  label: Text('3', style: TextStyle(color: Colors.white)),
                ),
              ],

              //
            ),
            line(context),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Login()));
              },
              child: titleContainer(
                  context, 'Log out', Icons.power_settings_new_rounded),
            ),
            line(context),
          ],
        ),
      ),
    );
  }
}

Widget line(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(top: 10, bottom: 10),
    color: Color(0xff666666),
    width: MediaQuery.of(context).size.width - 60,
    height: 1,
  );
}

Widget titleContainer(BuildContext context, String title, IconData icon) {
  return Container(
    width: MediaQuery.of(context).size.width - 60,
    height: 65,
    child: Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 20),
          child: Icon(
            icon,
            size: 32,
            color: Color(0xff7E2A6A),
          ),
        ),
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            // color: Color(0xff7E2A6A),
          ),
        ),
      ],
    ),
  );
}
