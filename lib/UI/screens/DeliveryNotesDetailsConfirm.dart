import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/Upload.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/subject_item.dart';
import 'package:get/get.dart';

class DeliveryNotesDetailsConfirmScreen extends StatefulWidget {
  @override
  _DeliveryNotesDetailsConfirmScreenState createState() => _DeliveryNotesDetailsConfirmScreenState();
}

class _DeliveryNotesDetailsConfirmScreenState extends State<DeliveryNotesDetailsConfirmScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  AppBar(
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        backgroundColor: Colors.white,

        title: Container(
          child: Text("Delivery note details confirm",style: TextStyle(color: Colors.black),),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * .05),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * .02,),
                Stack(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 150,
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 0),
                      padding: EdgeInsets.only(bottom: 10,right: 10,left: 10),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black
                        ),
                        borderRadius: BorderRadius.circular(20),
                        shape: BoxShape.rectangle,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            child: Container(
                              child: Icon(Icons.add,size: 80,color: Colors.black54,),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black54
                                  )
                              ),
                            ),
                            onTap: (){
                              Get.to(UploadScreen());
                            },
                          )
                        ],
                      ),
                    ),
                    Positioned(
                        left: 10,
                        top: 12,
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
                          color: Colors.white,
                          child: Row(
                            children: [
                              Icon(Icons.account_circle_outlined,color: Color(0xff7e2a6a),),
                              SizedBox(width: 5,),
                              Text(
                                'Detect Subjects',
                                style: TextStyle(color: Color(0xff7e2a6a), fontSize: 12),
                              )
                            ],
                          ),
                        )),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 150,
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 0),
                      padding: EdgeInsets.only(bottom: 10,right: 10,left: 10),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black
                        ),
                        borderRadius: BorderRadius.circular(20),
                        shape: BoxShape.rectangle,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            child: Container(
                              child: Icon(Icons.add,size: 80,color: Colors.black54,),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black54
                                  )
                              ),
                            ),
                            onTap: (){
                              Get.to(UploadScreen());
                            },
                          )
                        ],
                      ),
                    ),
                    Positioned(
                        left: 10,
                        top: 12,
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
                          color: Colors.white,
                          child: Row(
                            children: [
                              Icon(Icons.local_shipping_outlined,color: Color(0xff7e2a6a),),
                              SizedBox(width: 5,),
                              Text(
                                'Detect Vehicles',
                                style: TextStyle(color: Color(0xff7e2a6a), fontSize: 12),
                              )
                            ],
                          ),
                        )),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 150,
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 0),
                      padding: EdgeInsets.only(bottom: 10,right: 10,left: 10),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black
                        ),
                        borderRadius: BorderRadius.circular(20),
                        shape: BoxShape.rectangle,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            child: Container(
                              child: Icon(Icons.add,size: 80,color: Colors.black54,),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black54
                                  )
                              ),
                            ),
                            onTap: (){
                              Get.to(UploadScreen());
                            },
                          )
                        ],
                      ),
                    ),
                    Positioned(
                        left: 10,
                        top: 12,
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
                          color: Colors.white,
                          child: Row(
                            children: [
                              Icon(Icons.agriculture_rounded,color: Color(0xff7e2a6a),),
                              SizedBox(width: 5,),
                              Text(
                                'Detect Objects',
                                style: TextStyle(color: Color(0xff7e2a6a), fontSize: 12),
                              )
                            ],
                          ),
                        )),
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height*.02,),
                Container(
                  child: Center(
                    child: Column(

                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RaisedButton(
                              onPressed: (){},
                              textColor: Color(0xff7e2a6a),
                              color: Colors.white,
                              child: Text(
                                  "Cancel"
                              ),
                              shape:  RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color: Color(0xff7e2a6a),)
                              ),
                            ),

                            SizedBox(width: MediaQuery.of(context).size.width * 0.03,),
                            RaisedButton(
                              onPressed: (){
                                // Get.to(DeliveryNotesDetailsConfirmScreen());
                              },
                              color: Color(0xff7e2a6a),
                              textColor: Colors.white,
                              child: Text(
                                  "Submit"
                              ),
                              shape:  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),

                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],

            ),


          )


        ),
      ),
    );
  }
}
