import 'dart:async';
import 'package:flutter/material.dart';
import 'package:watch_area/UI//screens/Login.dart';
import 'package:watch_area/UI/screens/DomainRegisteration.dart';
import 'package:watch_area/UI/widgets/header.dart';
import 'package:watch_area/UI/widgets/screen_builder.dart';

import 'CollectionList.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StartState();
  }
}

class StartState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    navigateToHome();
  }

  navigateToHome() async {
    var duration = Duration(seconds: 5);
    print("test domain screen");
    return Timer(duration, routeToHome);
  }

  routeToHome() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => DomainRegistration()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: HeaderState(title: "DomainRegister", showTitle: false, height: 20),
      ),
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: [
          Center(
            child: Container(
              width: 200,
              child: new Image.asset(
                'assets/logo.png',
                // width: 600.0,
                // height: 240.0,
                // fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
