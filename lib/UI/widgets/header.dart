import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/MoreScreen.dart';

class HeaderState extends StatelessWidget {
  final String title;
  final bool showTitle;
  final double height;

  HeaderState({
    Key key,
    this.title,
    this.showTitle,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height,
      color: Color(0xff7E2A6A),

      child: ListView(
        children: [
          Container(
            height: height,
            width: MediaQuery.of(context).size.width,
          ),
          showTitle == true
              ? Container(
                  height: 51,
                  // padding: EdgeInsets.only(),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Icon(
                            Icons.power_settings_new_rounded,
                            size: 32,
                            color: Color(0xff7E2A6A),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Container(
                          // margin: EdgeInsets.only(top:5),
                          child: Text(title,
                              // textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                              )),
                        ),
                      ]),
                )
              : Container(),
        ],
      ),
    );
  }
}
