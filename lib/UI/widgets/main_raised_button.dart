import 'package:flutter/widgets.dart';

class MainRaisedButton extends StatelessWidget {
  final String title;
  final Color titleColor;
  final double fontSize;
  final Color backgroundColor;
  final double width;
  final double height;
  final double borderRadius;
  final Function onPressed;

  MainRaisedButton({
    Key key,
    this.title,
    this.titleColor,
    this.fontSize,
    this.backgroundColor,
    this.width,
    this.height,
    this.borderRadius,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius),
          color: backgroundColor,
        ),

        width: width,
        height: height,
        // margin: EdgeInsets.only(top:10, bottom: 15, left: 20),
        // width: 300,
        child: Center(
          child: Text(
            this.title,
            style: TextStyle(
              color: titleColor,
              fontSize: fontSize,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}
