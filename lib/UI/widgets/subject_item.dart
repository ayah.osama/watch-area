import 'package:flutter/material.dart';

class SubjectItemWidget extends StatefulWidget {
  final String subjectName;
  final bool done;

  const SubjectItemWidget({Key key, this.subjectName, this.done}) : super(key: key);
  @override
  _SubjectItemWidgetState createState() => _SubjectItemWidgetState();
}

class _SubjectItemWidgetState extends State<SubjectItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          widget.done ?
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: Colors.green,
                width: 2
              ),
            ),
            child: Icon(Icons.done,color: Colors.green,),
          ):
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                  color: Colors.red,
                  width: 2
              ),
            ),
            child: Icon(Icons.close,color: Colors.red,),
          ),
          SizedBox(width: 20,),
          Text(widget.subjectName,style:TextStyle(fontWeight: FontWeight.bold,color: Colors.black54),)
        ],
      ),
    );
  }
}
