import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/widgets/main_raised_button.dart';
import 'package:watch_area/UI/widgets/screen_builder.dart';

class LoginForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 15),
                // width: 300,
                child: Text(
                  "Enter username",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Container(
              child: Padding(
            padding: EdgeInsets.only(left: 10),
            child: TextFormField(
              validator: (value) {
                print(value);
                if (value == null || value == '') return "required field";
                return null;
              },
              decoration: new InputDecoration(
                border: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                fillColor: Color(0xffECEFF1),
                filled: true,
                // borderRadius: BorderRadius.circular(20),
                prefixIcon: Container(
                  child: Icon(
                    Icons.account_circle,
                    size: 30,
                  ),
                ),

                labelText: 'example@example.com',
                labelStyle: TextStyle(
                  color: Color(0xffB8B6B6),
                ),
              ),
            ),
          )),

          Row(
            children: [
              Container(
                margin: EdgeInsets.only(top: 30, bottom: 15),
                // width: 300,
                child: Text(
                  "Enter password",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),

          Container(
              child: Padding(
            padding: EdgeInsets.only(left: 10),
            child: TextFormField(
              obscureText: true,
              validator: (value) {
                print(value);
                if (value == null || value == '') return "required field";
                return null;
              },
              decoration: new InputDecoration(
                border: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                fillColor: Color(0xffECEFF1),
                filled: true,
                // borderRadius: BorderRadius.circular(20),
                prefixIcon: Container(
                  child: Icon(
                    Icons.lock,
                    color: Colors.black,
                    size: 30,
                  ),
                ),

                labelText: 'Password',
                labelStyle: TextStyle(
                  color: Color(0xffB8B6B6),
                ),
              ),
            ),
          )),
          // Container(
          //     decoration: BoxDecoration(
          //       color: Color(0xffECEFF1),
          //       borderRadius: BorderRadius.circular(20),
          //     ),
          //     child: Padding(
          //       padding: EdgeInsets.only(left: 10),
          //       child: TextFormField(
          //         obscureText: true,
          //         // autocorrect: false,
          //         // focusNode: _focusNode,
          //         // style: TextStyle(color: Colors.green),
          //         decoration: new InputDecoration(
          //           icon: Icon(
          //             Icons.lock,
          //             color: Colors.black,
          //             size: 30,
          //           ),
          //           border: InputBorder.none,
          //           labelText: 'Password',
          //           labelStyle: TextStyle(
          //             color: Color(0xffB8B6B6),
          //           ),
          //         ),
          //       ),
          //     )),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  print('test forget password');
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10, bottom: 80, left: 20),
                  // width: 300,
                  child: Text(
                    "Forget Password ?",
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff057CE3),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Center(
            child: MainRaisedButton(
              title: 'Sign In',
              titleColor: Color(0xffFFFFFF),
              fontSize: 25,
              backgroundColor: Color(0xff7E2A6A),
              width: MediaQuery.of(context).size.width - 50,
              height: 50,
              borderRadius: 20,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ScreenBuilder()));

                print('sign in function');
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () {
                  print('test change connection data');
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10, bottom: 40, left: 20),
                  // width: 300,
                  child: Text(
                    "change connection data ?",
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff057CE3),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ]));
  }
}
