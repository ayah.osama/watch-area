import 'package:flutter/cupertino.dart';
import 'package:watch_area/UI/screens/DeliveryNotesDetails.dart';
import 'package:watch_area/UI/screens/MoreScreen.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:watch_area/UI/screens/CollectionDetails.dart';

class MainCard extends StatelessWidget {
  final int cardId;

  // final String title;
  // final Color fontColor;
  // final Color titleColor;
  // final double fontSize;
  final Color backgroundColor;
  final double width;
  final double height;
  final double borderRadius;
  final Widget child;

  MainCard({
    Key key,
    this.cardId,
    // this.title,
    // this.fontColor,
    // this.titleColor,
    // this.fontSize,
    this.backgroundColor,
    this.width,
    this.height,
    this.borderRadius,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(DeliveryNotesDetailsScreen());
        print('Carddddddddddddddddd');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CollectionDetails()));

      },
      child: Container(
        padding: EdgeInsets.only(top:13, left: 13, bottom: 13),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius),
          color: backgroundColor,
        ),
        width: width,
        // height: height,
        // margin: EdgeInsets.only(top:10, bottom: 15, left: 20),
        // width: 300,
        child: child,
        // SafeArea(
        //   child:
        // ),
      ),
    );
  }
}
