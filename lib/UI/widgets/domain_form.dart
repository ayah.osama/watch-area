import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:watch_area/UI/screens/Login.dart';
import 'package:watch_area/UI/widgets/main_raised_button.dart';

class DomainForm extends StatefulWidget {
  @override
  DomainFormState createState() {
    return DomainFormState();
  }
}

enum Protocol { Http, Https }
enum UserType { Domain, Local }

class DomainFormState extends State<DomainForm> {
  final _formKey = GlobalKey<FormState>();
  Protocol _protocol = Protocol.Http;
  UserType _userType = UserType.Domain;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 15),
                // width: 300,
                child: Text(
                  "Enter server name",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Container(
              child: Padding(
            padding: EdgeInsets.only(left: 10),
            child: TextFormField(
              validator: (value) {
                print(value);
                if (value == null || value == '') return "required field";
                return null;
              },
              decoration: new InputDecoration(
                border: UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                fillColor: Color(0xffECEFF1),
                filled: true,
                // borderRadius: BorderRadius.circular(20),
                prefixIcon: Container(
                  child: Icon(
                    Icons.auto_awesome_motion,
                    size: 30,
                  ),
                ),

                labelText: 'Http:\\example',
                labelStyle: TextStyle(
                  color: Color(0xffB8B6B6),
                ),
              ),
            ),
          )),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(top: 30, bottom: 15),
                // width: 300,
                child: Text(
                  "Connection protocol",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: SizedBox(
                  // width: MediaQuery.of(context).size.width-100,
                  child: ListTile(
                    title: const Text('HTTP',
                    style: TextStyle(fontSize: 15),),
                    leading: Radio(
                      activeColor: Colors.black,
                      value: Protocol.Http,
                      groupValue: _protocol,
                      onChanged: (Protocol value) {
                        setState(() {
                          _protocol = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Text('Port number'),
              SizedBox(
                width: 5,
              ),
              Expanded(
                  child: Container(
                color: Color(0xffECEFF1),
                child: TextFormField(
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    labelStyle: TextStyle(
                      color: Color(0xffB8B6B6),
                    ),
                  ),
                ),
              )),
            ],
          ),
          ListTile(
            title: const Text('HTTPS',
              style: TextStyle(fontSize: 15),),
            leading: Radio(
              activeColor: Colors.black,
              value: Protocol.Https,
              groupValue: _protocol,
              onChanged: (Protocol value) {
                setState(() {
                  _protocol = value;
                });
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                // margin: EdgeInsets.only(bottom: 15),
                // width: 300,
                child: Text(
                  "User type",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                  child: SizedBox(
                child: ListTile(
                  title: const Text('Domain'),
                  leading: Radio(
                    activeColor: Colors.black,
                    value: UserType.Domain,
                    groupValue: _userType,
                    onChanged: (UserType value) {
                      setState(() {
                        _userType = value;
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                  child: SizedBox(
                child: ListTile(
                  title: const Text('Local'),
                  leading: Radio(
                    activeColor: Colors.black,
                    value: UserType.Local,
                    groupValue: _userType,
                    onChanged: (UserType value) {
                      setState(() {
                        _userType = value;
                      });
                    },
                  ),
                ),
              ))
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Center(
            child: MainRaisedButton(
              title: 'Next',
              titleColor: Color(0xffFFFFFF),
              fontSize: 25,
              backgroundColor: Color(0xff7E2A6A),
              width: MediaQuery.of(context).size.width - 50,
              height: 50,
              borderRadius: 20,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                print('Domain function');
                // if (_formKey.currentState.validate()) {
                //   print('valid');
                // } else {
                //   print('not valid');
                // }
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ]));
  }
}
