import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:watch_area/UI/screens/CaptureScreen.dart';
import 'package:watch_area/UI/screens/CollectionList.dart';
import 'package:watch_area/UI/screens/MoreScreen.dart';
import 'package:watch_area/UI/screens/Notifications.dart';

class ScreenBuilderState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenBuilder();
  }
}

class ScreenBuilder extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ScreenBuilder> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    // CaptureScreen(),
    Notifications(),
    Notifications(),
    // CollectionListScreen(),
    MoreScreen(),
  ];

  changeIndex() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: Color(0xffe8e8e9),
        selectedIndex: _currentIndex,
        showElevation: false,
        itemCornerRadius: 24,
        curve: Curves.easeIn,
        onItemSelected: (index) => {
          setState(() => _currentIndex = index),
          print(_currentIndex),
        },
        items: <BottomNavyBarItem>[
          // BottomNavyBarItem(
          //   icon: Icon(Icons.camera_alt),
          //   title: Text('Capture'),
          //   activeColor: Color(0xff7E2A6A),
          //   textAlign: TextAlign.center,
          //   inactiveColor: Color(0xff222226),
          // ),
          BottomNavyBarItem(
            icon: Icon(Icons.notifications_rounded),
            title: Text('Notifications'),
            activeColor: Color(0xff7E2A6A),
            textAlign: TextAlign.center,
            inactiveColor: Color(0xff222226),
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.account_circle),
            title: Text(
              'Profile',
            ),
            activeColor: Color(0xff7E2A6A),
            textAlign: TextAlign.center,
            inactiveColor: Color(0xff222226),
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.more_vert_sharp),
            title: Text('More'),
            activeColor: Color(0xff7E2A6A),
            textAlign: TextAlign.center,
            inactiveColor: Color(0xff222226),
          ),
        ],
      ),
    );
  }
}
